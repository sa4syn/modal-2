import './App.css';
import Modal from './components/Modal';
import useModal from './components/useModal';
import { useEffect } from 'react';

function App() {
  const basicModal = useModal();
  useEffect(() => {
}
   , [])
  return (
    <div className="App">
      <Modal show={basicModal.display} close={basicModal.toggle}>
          <p>Modal</p>
        </Modal>
        <button type='button' onClick={() => basicModal.toggle()}>Show Modal</button>
    </div>
  );
}

export default App;
