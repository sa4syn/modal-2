import './style.css';

const Modal = ({ close, show, children }) => {
  return (
    show ?
      <>
        <div className='modal' onClick={close}></div>
        <section className="modal-main" >
          {children}
          <button type="button" onClick={close}>
            Close
          </button>
        </section>
      </>
      : null
  );
};

export default Modal