import { useCallback, useState } from 'react';

const useModal = () => {
  const [display, setDisplay] = useState(false);
  const toggle = useCallback(() => setDisplay(!display), [display])
  return {
    display,
    toggle,
  }
};

export default useModal;